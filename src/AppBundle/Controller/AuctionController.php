<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Auction;
use AppBundle\Form\AuctionType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Description of AuctionController
 *
 * @author ncoderpl
 */
class AuctionController extends Controller
{

    /**
     * @Route("/", name="auction_index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $auctions = $entityManager->getRepository(Auction::class)->findAll();
        return $this->render("Auction/index.html.twig", ["auctions" => $auctions]);
    }

    /**
     * @Route("/{id}", name="auction_detail")
     *
     * @param Auction $auction
     *
     * @return Response
     */
    public function detailsAction(Auction $auction)
    {
//        $entitymanager = $this->getDoctrine()->getManager();
//        $auction = $entitymanager->getRepository(Auction::class)->findOneBy(["id" => $id]);

        $deleteForm = $this->createFormBuilder()
            ->setAction($this->generateUrl("auction_delete", ["id" => $auction->getId()]))
            ->setMethod(Request::METHOD_DELETE)
            ->add("submit", SubmitType::class, ['label' => 'usuń'])
            ->getForm();
        return $this->render(
            "Auction/details.html.twig",
            [
                "message" => "Opisy aukcji",
                "auction" => $auction,
                "deleteForm" => $deleteForm->createView()
            ]
        );
    }

    /**
     * @Route("/auction/add", name="auction_add")
     *
     * @return Response
     */
    public function addAction(Request $request)
    {
        $auction = new Auction();

        $form = $this->createForm(AuctionType::class, $auction);

        if ($request->isMethod("post")) {

            $form->handleRequest($request);
            if($form->isValid()) {
                $auction
                    ->setCreatedAt(new DateTime())
                    ->setUpdatedAt(new DateTime())
                    ->setStatus(Auction::STATUS_ACTIVE);

                $auction = $form->getData();

                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->persist($auction);

                $entityManager->flush();

                return $this->redirectToRoute("auction_index");
            }
        }

        return $this->render("Auction/add.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @Route("/auction/edit/{id}", name="auction_edit")
     *
     * @param Request $request
     *
     * @param Auction $auction
     *
     * @return Response
     */
    public function editAction(Request $request, Auction $auction)
    {
        $form = $this->createForm(AuctionType::class, $auction);

        if ($request->isMethod("post")) {

            $form->handleRequest($request);

            $auction->setUpdatedAt(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($auction);

            $entityManager->flush();

            return $this->redirectToRoute("auction_detail", ["id" => $auction->getId()]);
        }

        return $this->render("Auction/edit.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @Route("/auction/delete/{id}", name="auction_delete", methods={"DELETE"})
     *
     * @param Auction $auction
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Auction $auction)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($auction);

        $entityManager->flush();


        return $this->redirectToRoute("auction_index");
    }

}
