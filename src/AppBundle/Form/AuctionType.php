<?php

namespace AppBundle\Form;

use AppBundle\Entity\Auction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of AuctionType
 *
 * @author ncoderpl
 */
class AuctionType extends AbstractType
{

    public function buildForm(FormBuilderInterface $formBuilder, array $options)
    {

        $formBuilder
            ->add('title', TextType::class, ["label" => "Marka"])
            ->add('desctiption', TextType::class, ["label" => "Opis auta"])
            ->add('starting_price', NumberType::class, ["label" => "Cena wywoławcza"])
            ->add('price', NumberType::class, ["label" => 'Cena "Kup teraz"'])
            ->add('expires', DateTimeType::class, ["label" => "Data zakończenia aukcji"])
            ->add('submit', SubmitType::class, ["label" => "Zapisz"]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(["data_class" => Auction::class]);
    }

}
